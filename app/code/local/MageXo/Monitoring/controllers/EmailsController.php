<?php

class MageXo_Monitoring_EmailsController extends Mage_Core_Controller_Front_Action {

    /** @var Mage_Core_Model_Email_Queue */
    private $coreEmailQueue;

    public function _construct() {
        if(is_null($this->coreEmailQueue)) {
            $this->coreEmailQueue = Mage::getModel('core/email_queue');
        }
    }

    public function indexAction() {
        $now = new DateTime();
        $from = (string)$now->sub(new DateInterval('PT15M'))->format('Y-m-d H:i:s');

        $isNotNull = ['notnull' => true];
        $olderThen = ["lt" => $from];

        $collection = $this->coreEmailQueue
            ->getCollection()
            ->addFieldToFilter('created_at', $olderThen)
            ->addFieldToFilter('processed_at', $isNotNull);

        echo count($collection) ? 'OK' : 'NOR';
    }
}
