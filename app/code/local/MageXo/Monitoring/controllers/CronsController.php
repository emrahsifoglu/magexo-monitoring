<?php

class MageXo_Monitoring_CronsController extends Mage_Core_Controller_Front_Action {

    /** @var Mage_Cron_Model_Schedule cronSchedules */
    private $cronSchedule;

    public function _construct() {
        if(is_null($this->cronSchedule)) {
            $this->cronSchedule = Mage::getModel('cron/schedule');
        }
    }

    public function indexAction() {
        $now = new DateTime();
        $to = (string)$now->format('Y-m-d H:i:s');
        $from = (string)$now->sub(new DateInterval('PT15M'))->format('Y-m-d H:i:s');

        $range = [
            'from' => $from,
            'to' => $to
        ];
        
        $collection = $this->cronSchedule
            ->getCollection()
            ->addFieldToFilter('executed_at', $range);

        echo count($collection) ? 'OK' : 'NOR';
    }

}
